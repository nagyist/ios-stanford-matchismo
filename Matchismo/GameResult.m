//
//  GameResult.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-17.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "GameResult.h"

#define ALL_RESULTS_KEY @"GameResult_All"
#define START_KEY @"StartDate"
#define END_KEY @"EndDate"
#define SCORE_KEY @"Score"

@interface GameResult()
@property (readwrite, nonatomic) NSDate *start;
@property (readwrite, nonatomic) NSDate *end;
@end

@implementation GameResult

+ (NSArray *)allGameResults
{
  NSMutableArray *allGameResults = [[NSMutableArray alloc] init];

  for (id plist in [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] allValues]) {
    GameResult *result = [[GameResult alloc] initFromPropertyList:plist];
    [allGameResults addObject:result];
  }

  return allGameResults;
}

// convenience initializer
// converts a 'frozen' game result from a propertylist
// to an actual GameResult object
- (id)initFromPropertyList:(id)plist
{
  self = [self init];
  if (self){
    if ([plist isKindOfClass:[NSDictionary class]]) {
      NSDictionary *resultDictionary = (NSDictionary *)plist;
      _start = resultDictionary[START_KEY];
      _end = resultDictionary[END_KEY];
      _score = [resultDictionary[SCORE_KEY] intValue];  // _score needs an int
      if (!_start || !_end) self = nil;
    }
  }
  return self;
}

- (void)synchronize
{
  NSMutableDictionary *mutableGameResultsFromUserDefaults = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] mutableCopy];
  if (!mutableGameResultsFromUserDefaults) mutableGameResultsFromUserDefaults = [[NSMutableDictionary alloc] init];

  // use the description of the self.start (NSDate) as the key for this dictionary
  // and store within it this game result as a property list
  mutableGameResultsFromUserDefaults[[self.start description]] = [self asPropertyList];
  [[NSUserDefaults standardUserDefaults] setObject:mutableGameResultsFromUserDefaults forKey:ALL_RESULTS_KEY];
  [[NSUserDefaults standardUserDefaults] synchronize];
}


- (id)asPropertyList
{
  // note SCORE_KEY value must be a number, hence @() for conversion
  return @{ START_KEY : self.start, END_KEY : self.end, SCORE_KEY : @(self.score) };
}

// designated initializer
- (id)init
{
  self = [super init];
  if (self) {
    _start = [NSDate date];
    _end = _start;
  }

  return self;
}

- (NSTimeInterval)duration {
  return [self.end timeIntervalSinceDate: self.start];
}

- (void)setScore:(int)score {
  _score = score;

  // also set the end date after the score has been set
  // assumes game over
  self.end = [NSDate date];

  [self synchronize];
}

@end
