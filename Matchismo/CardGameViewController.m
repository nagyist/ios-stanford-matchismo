//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-15.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "CardGameViewController.h"
#import "CardMatchingGame.h"
#import "GameResult.h"

@interface CardGameViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
// note: why not of type PlayingCardDeck?
// only use what is required by this controller's implementation
// if controller doesn't use methods specific to PlayingCardDeck
// just use its superclass -- makes this controller more transferrable

// ** LESSON 3 removed the 'deck' property below
// @property (strong, nonatomic) Deck *deck;

@property (strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) GameResult *gameResult;

@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@end

@implementation CardGameViewController

#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return self.startingCardCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlayingCard" forIndexPath:indexPath];

  // recall an indexpath has properties: section, item
  Card *card = [self.game cardAtIndex:indexPath.item];
  [self updateCell:cell usingCard:card];

  return cell;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)Card
{
  // abstract
}


- (GameResult *)gameResult
{
  if (!_gameResult) _gameResult = [[GameResult alloc] init];

  return _gameResult;
}

- (CardMatchingGame *)game
{
  if (!_game) _game = [[CardMatchingGame alloc] initWithCardCount: self.startingCardCount
                                                        usingDeck: [self createDeck]];

  return _game;
}

- (Deck *)createDeck { return nil; }  // ABSTRACT (does nothing unless overridden/implemented by other subclasses of this class)

// This is the controller's fundamental task:
// synchronizes/matches UI whenever model changes
- (void)updateUI
{
  for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells]) {
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
    Card *card = [self.game cardAtIndex:indexPath.item];
    [self updateCell:cell usingCard:card];
  }
  self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
}

- (IBAction)flipCard:(UITapGestureRecognizer *)gesture
{
  // after refactoring
  // *** LESSON 3 removed line below:
  // sender.selected = !sender.isSelected;

  // before refactoring
  //  if (sender.isSelected) {
  //    sender.selected = NO;
  //  }
  //  else {
  //    sender.selected = YES;
  //  }

  // get the card that needs to be flipped based on the tap gesture
  CGPoint tapLocation = [gesture locationInView:self.cardCollectionView];
  NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];

  // ensure that tap was actually registered on a card
  if (indexPath) {
    [self.game flipCardAtIndex:indexPath.item];
    self.flipCount++;

    // update UI after a flip
    [self updateUI];

    // update score
    self.gameResult.score = self.game.score;
  }
}

- (void)setFlipCount:(int)flipCount
{
  _flipCount = flipCount;
  self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

// Basically resets the game
- (IBAction)deal {
  self.game = nil;
  self.gameResult = nil;
  self.flipCount = 0;
  [self updateUI];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
