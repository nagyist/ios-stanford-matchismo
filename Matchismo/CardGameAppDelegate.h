//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-15.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
