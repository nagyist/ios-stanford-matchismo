//
//  main.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-15.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
  }
}
