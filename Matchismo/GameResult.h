//
//  GameResult.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-17.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameResult : NSObject

+ (NSArray *)allGameResults;

@property (readonly, nonatomic) NSDate *start;
@property (readonly, nonatomic) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval duration;
@property (nonatomic) int score;

@end
