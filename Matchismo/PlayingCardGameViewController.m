//
//  PlayingCardGameViewController.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-08-07.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h" 
#import "PlayingCardCollectionViewCell.h"

@implementation PlayingCardGameViewController

- (NSUInteger)startingCardCount
{
  return 20;
}

- (Deck *)createDeck
{
  return [[PlayingCardDeck alloc] init];
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card
{
  // only handle cases where cell is the kind of class we want (PlayingCardCollectionViewCell)
  if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]]) {

    // reference the view out of cell
    PlayingCardView *playingCardView = ((PlayingCardCollectionViewCell *)cell).playingCardView;

    // if we have a playing card, set its suit, rank, faceup and alpha properties
    if ([card isKindOfClass:[PlayingCard class]]) {
      PlayingCard *playingCard = (PlayingCard *)card;
      playingCardView.rank = playingCard.rank;
      playingCardView.suit = playingCard.suit;
      playingCardView.faceUp = playingCard.isFaceUp;
      playingCardView.alpha = playingCard.isUnplayable ? 0.3 : 1.0;
     
    }
  }
}

@end
