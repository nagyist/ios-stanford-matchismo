//
//  PlayingCardCollectionViewCell.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-08-07.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardView.h"

@interface PlayingCardCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;

@end
